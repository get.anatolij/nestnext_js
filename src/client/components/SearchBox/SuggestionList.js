import React from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
// import classNames from 'classnames';

const SuggestionList = ({ dataItemKey, suggestions, search }) => {
  let items = [];

  const makeSuggestions = () => {
    const regex = new RegExp(_.escapeRegExp(search), 'i');
    const result = [];
    suggestions
      .filter((item) => !_.isEmpty(item[dataItemKey]))
      .forEach((item) => {
        const value = item[dataItemKey];
        const matches = regex.exec(value);
        const suggestion = {
          ...item,
          distinctField: dataItemKey,
        };

        if (matches) {
          result.push(suggestion);
        }
      });
    return result;
  };

  return (
    <div className="search-list">
      <ul>
        {!items.length &&
          makeSuggestions(items).map((item, index) => (
            <li key={index}>{item[dataItemKey]}</li>
          ))}
      </ul>
    </div>
  );
};

SuggestionList.propTypes = {
  className: PropTypes.string,
  dataItemKey: PropTypes.string,
  search: PropTypes.string,
  suggestions: PropTypes.arrayOf(Object),
};

SuggestionList.defaultProps = {
  dataItemKey: 'id',
  position: 'right',
};

SuggestionList.displayName = 'SuggestionList';

export default SuggestionList;
