import PropTypes from 'prop-types';

import React from 'react';
import Slider from 'react-slick';

const Slides = () => {
  const settings = {
    dots: true,
    dotsClass: 'my-dots',
    infinite: true,
    lazyLoad: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 6000,
    className: 'slides',
    pauseOnHover: true,
  };

  return (
    <div className="slides">
      <Slider {...settings}>
        <div>
          <img src="/images/image1.jpeg" alt="ss" />
          <div className="slides-text">
            <h3>Title</h3>
            <h4>Description</h4>
          </div>
        </div>
        <div>
          <img src="/images/image2.jpg" alt="ss" />
          <div className="slides-text">
            <h3>Title</h3>
            <h4>Description</h4>
          </div>
        </div>
        <div>
          <img src="/images/image3.jpeg" alt="ss" />
          <div className="slides-text">
            <h3>Title</h3>
            <h4>Description</h4>
          </div>
        </div>
        <div>
          <img src="/images/image4.jpeg" alt="ss" />
          <div className="slides-text">
            <h3>Title</h3>
            <h4>Description</h4>
          </div>
        </div>
        <div>
          <img src="/images/image3.jpeg" alt="ss" />
          <div className="slides-text">
            <h3>Title</h3>
            <h4>Description</h4>
          </div>
        </div>
        <div className="video">
          <video controls autoPlay muted loop>
            <source src="/videotest.mp4" type="video/mp4" />
          </video>
          <div className="slides-text">
            <h3>Title</h3>
            <h4>Description</h4>
          </div>
        </div>
      </Slider>
    </div>
  );
};

Slides.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export default Slides;
