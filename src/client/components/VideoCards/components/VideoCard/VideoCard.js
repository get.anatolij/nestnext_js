import PropTypes from 'prop-types';

import React from 'react';
import { Button } from '../../../Button';
import classNames from 'classnames';

const VideoCard = ({ className }) => {
  return (
    <div className={classNames('videoCard', className)}>
      <div className="videoCard-text">
        <h3>Title</h3>
        <h4>Description</h4>
      </div>
      <div className="videoCard-button">
        <Button className={'wise-button-standard'}>Watch</Button>
      </div>
    </div>
  );
};

VideoCard.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default VideoCard;
