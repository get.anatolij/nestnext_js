import { Button } from '../Button';
import { ButtonIcon } from '../ButtonIcon';
import Link from 'next/link';

const section1 = {
  title: 'Help',
  items: [
    {
      title: 'FAQ',
      url: '/faq',
    },
    {
      title: 'Tutorials',
      url: '/tutorials',
    },
    {
      title: 'youtube/teamwise',
      url: 'https://www.youtube.com/',
      icon: (
        <span className="icon-youtube">
          <span className="path1" />
          <span className="path2" />
        </span>
      ),
    },
    {
      title: 'vimeo/teamwise',
      url: 'https://vimeo.com/',
      icon: (
        <span className="icon-vimeo">
          <span className="path1" />
          <span className="path2" />
        </span>
      ),
    },
    {
      title: 't.me/teamwise',
      url: 'https://desktop.telegram.org/',
      icon: (
        <span className="icon-telegram">
          <span className="path1" />
          <span className="path2" />
        </span>
      ),
    },
    {
      title: 'Pricing',
      url: '/pricing',
    },
  ],
};
const section2 = {
  title: 'Contacts',
  items: [
    {
      title: 'Social media',
      url: '/media',
    },
    {
      icons: [
        {
          url: 'https://www.instagram.com/',
          icon: (
            <span className="icon-instagram">
              <span className="path1" />
              <span className="path2" />
              <span className="path3" />
            </span>
          ),
        },
        {
          url: 'https://www.youtube.com/',
          icon: (
            <span className="icon-youtube">
              <span className="path1" />
              <span className="path2" />
            </span>
          ),
        },
        {
          url: 'https://www.linkedin.com/',
          icon: (
            <span className="icon-linkedin">
              <span className="path1" />
              <span className="path2" />
              <span className="path3" />
              <span className="path4" />
            </span>
          ),
        },
        {
          url: 'https://twitter.com/',
          icon: (
            <span className="icon-twitter">
              <span className="path1" />
              <span className="path2" />
            </span>
          ),
        },
        {
          url: 'https://www.facebook.com/',
          icon: (
            <span className="icon-facebook">
              <span className="path1" />
              <span className="path2" />
            </span>
          ),
        },
      ],
    },
    {
      title: 'Tech support',
      url: '/support',
    },
    {
      title: 'info@teamwiseapp.com',
      url: 'http://localhost',
      icon: (
        <span className="icon-email">
          <span className="path1" />
          <span className="path2" />
          <span className="path3" />
        </span>
      ),
    },
    {
      title: 'whatsapp/teamwise',
      url: 'https://www.whatsapp.com/',
      icon: (
        <span className="icon-whattsapp">
          <span className="path1" />
          <span className="path2" />
        </span>
      ),
    },
    {
      title: 't.me/teamwise',
      url: 'https://desktop.telegram.org/',
      icon: (
        <span className="icon-telegram">
          <span className="path1" />
          <span className="path2" />
        </span>
      ),
    },
  ],
};
const section3 = {
  title: 'Legal',
  items: [
    {
      title: 'Privacy Policy',
      url: '/policy',
    },
    {
      title: 'Terms & conditions',
      url: '/terms',
    },
    {
      title: 'License agreement',
      url: '/license',
    },
    {
      title: 'Copyright information',
      url: '/information',
    },
    {
      title: 'Cookies policy',
      url: '/cookiesPolicy',
    },
  ],
};

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-wrapper">
        <div className="footer-wrapper-list">
          <h3>{section1.title}</h3>
          <div className="footer-wrapper-list__links">
            {section1.items.map(({ icon, title, url }, index) => {
              if (icon) {
                return (
                  <Link href={url}>
                    <ButtonIcon
                      key={index}
                      title={title}
                      icon={icon}
                      className="wise-button-icon-footer"
                    />
                  </Link>
                );
              } else {
                return (
                  <Link href={url}>
                    <Button key={index} className="wise-button-footer">
                      {title}
                    </Button>
                  </Link>
                );
              }
            })}
          </div>
        </div>
        <div className="footer-wrapper-list">
          <h3>{section2.title}</h3>
          <div className="footer-wrapper-list__links">
            {section2.items.map(({ icon, title, icons, url }, index) => {
              if (icon) {
                return (
                  <Link href={url}>
                    <ButtonIcon
                      key={index}
                      title={title}
                      icon={icon}
                      className="wise-button-icon-footer"
                    />
                  </Link>
                );
              } else if (icons) {
                return (
                  <div className="footer-wrapper-list-social">
                    {icons.map(({ icon, url }, index) => {
                      return (
                        <Link href={url} key={index}>
                          <ButtonIcon
                            key={index}
                            icon={icon}
                            className="wise-button-icon-footer"
                          />
                        </Link>
                      );
                    })}
                  </div>
                );
              } else {
                return (
                  <Link href={url}>
                    <Button key={index} className="wise-button-footer">
                      {title}
                    </Button>
                  </Link>
                );
              }
            })}
          </div>
        </div>
        <div className="footer-wrapper-list">
          <h3>{section3.title}</h3>
          <div className="footer-wrapper-list__links">
            {section3.items.map(({ icon, title, url }, index) => {
              if (icon) {
                return (
                  <Link href={url}>
                    <ButtonIcon
                      key={index}
                      title={title}
                      icon={icon}
                      className="wise-button-icon-footer"
                    />
                  </Link>
                );
              } else {
                return (
                  <Link href={url}>
                    <Button key={index} className="wise-button-footer">
                      {title}
                    </Button>
                  </Link>
                );
              }
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
