import PropTypes from 'prop-types';
import Image from 'next/image';
import { Button } from '../Button';
import { useState } from 'react';
import { ButtonIcon } from '../ButtonIcon';
import { SearchBox } from '../SearchBox';
import Popup from './Popup';
import Link from 'next/link';

const Header = ({ title, logo }) => {
  const [isAuth, setIsAuth] = useState(true);
  const [choiceBtnIcon, setChoiceBtnIcon] = useState('');
  const Login = () => {
    setIsAuth(!isAuth);
  };

  return (
    <div className="wise-header">
      <div className="wise-header-wrapper">
        <div className="wise-header-wrapper__logo">{logo}</div>
        <Link href="/">
          <div className="wise-header-wrapper__title">{title}</div>
        </Link>
        {isAuth && (
          <>
            <ButtonIcon
              icon={<span className="icon-Media" />}
              className="wise-button-icon-header wise-header-wrapper__iconMedia"
              setChoiceBtnIcon={setChoiceBtnIcon}
              choiceBtnIcon={choiceBtnIcon}
            >
              <div className="badges">!</div>
              <span
                className={
                  choiceBtnIcon === 'media' ? 'icon-Media active' : 'icon-Media'
                }
              />
            </ButtonIcon>
            <ButtonIcon
              icon={<span className="icon-Settings" />}
              className="wise-button-icon-header wise-header-wrapper__iconSettings"
            >
              <div className="badges">!</div>
              <span className="icon-Scene_settings" />
            </ButtonIcon>
          </>
        )}
        <SearchBox
          dataItemKey="value"
          suggestions={[{ value: 'funk' }, { value: 'test' }]}
          isAuth={isAuth}
        />
        {/*{children}*/}
        {isAuth && (
          <>
            <ButtonIcon
              icon={<span className="icon-Bell" />}
              className="wise-button-icon-header wise-header-wrapper__iconBell"
            >
              <div className="badges">!</div>
              <span className="icon-Bell" />
            </ButtonIcon>
            <ButtonIcon
              icon={<span className="icon-Message" />}
              className="wise-button-icon-header wise-header-wrapper__iconMessage"
            >
              <div className="badges">!</div>
            </ButtonIcon>
            <ButtonIcon
              icon={
                <span className="icon-Logo2">
                  <span className="path1" />
                  <span className="path2" />
                  <span className="path3" />
                </span>
              }
              className="wise-button-icon-header wise-header-wrapper__iconLogo"
            >
              <div className="badges">!</div>
            </ButtonIcon>
          </>
        )}
        {!isAuth && (
          <>
            <Button
              className=" wise-button-link wise-header-wrapper__btnLogin"
              onClick={() => {
                setIsAuth(!isAuth);
              }}
            >
              Log In
            </Button>
            <Button className=" wise-button-link" onClick={() => {}}>
              Sign up free
            </Button>
          </>
        )}
        <>
          <div
            className={
              !isAuth
                ? 'wise-header-wrapper__user'
                : 'wise-header-wrapper__userIsAuth'
            }
            onClick={Login}
          >
            <Image src="/iconUser.svg" alt="Search" width={24} height={24} />
          </div>
        </>
      </div>
      <Popup/>
    </div>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  logo: PropTypes.element,
  children: PropTypes.element,
};

export default Header;
